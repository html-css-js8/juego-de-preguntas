    let baseDePregunta = [
        {
            pregunta: "¿Quién es el maximo ganador del balón de oro?",
            imagen: "https://i.ibb.co/cL7T290/balon-oro.jpg",
            ayuda: "El jugador es Argentino",
            ayudaImg: "https://i.ibb.co/kcfXh7c/bandera-argentina.jpg",
            respuesta: "Lionel Messi",
            distractores: ["Crisitiano Ronaldo", "Rafael Nadal", "Andres Iniesta"],
        },
        {
            pregunta: "¿Como se llama el estadio del Real Madrid?",
            imagen: "https://i.ibb.co/b7MFSCm/sb.jpg",
            ayuda: "El estadio se encuentra en Madrid España",
            ayudaImg: "https://i.ibb.co/59K8dtG/rm.jpg",
            respuesta: "Santiago Bernabeu",
            distractores: ["Old Traford", "Alfredo Di Estefano", "Camp Nou"],
        },
        {
            pregunta: "¿Cuál es la nacionalidad de Cristiano Ronaldo?",
            imagen: "https://i.ibb.co/xzbCQyL/cr7.jpg",
            respuesta: "Portugal",
            distractores: ["España", "Alemania", "Holanda"],
        },
    ];