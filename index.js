/* CARGAR PREGUNTAS */
    let INDEX_PREGUNTA = 0;
    let puntaje = 0;

    CargarPregunta(0)

    function CargarPregunta(index) {
        objetoPregunta = baseDePregunta[index];
        opciones = [...objetoPregunta.distractores];
        opciones.push(objetoPregunta.respuesta);
        for (let i = 0; i < 4; i++) {
            opciones.sort(() => Math.random() - 0.5);
        }

        if (objetoPregunta.ayuda) {
            document.getElementById("ayuda").style.display = "";
          } else {
            document.getElementById("ayuda").style.display = "none";
          }

        document.getElementById("pregunta").innerHTML = objetoPregunta.pregunta;
        document.getElementById("imagen").src = objetoPregunta.imagen;

        document.getElementById("opción-1").innerHTML = opciones[0];
        document.getElementById("opción-2").innerHTML = opciones[1];
        document.getElementById("opción-3").innerHTML = opciones[2];
        document.getElementById("opción-4").innerHTML = opciones[3];
    }
    /* Validez de respuesta correcta */
    async function seleccionarOpcion(index){
        let validezRespuesta = opciones[index] == objetoPregunta.respuesta;
        if (validezRespuesta) {
            await Swal.fire({
                title: "Respuesta correcta",
                text: "La respuesta ha sido correcta",
                icon: "success",
            });
            puntaje ++;
        } else {
            await Swal.fire({
                title: "Respuesta incorrecta",
                text: `La respuesta correcta es: "${objetoPregunta.respuesta}"`,
                icon: "error",
            });
        }
        INDEX_PREGUNTA++;
        if (INDEX_PREGUNTA >= baseDePregunta.length) {
            await Swal.fire({
              title: "Juego términado",
              text: `Tu puntaje fue de: ${puntaje}/${baseDePregunta.length}`,
            });
            INDEX_PREGUNTA = 0;
            puntaje = 0;
          }
        CargarPregunta(INDEX_PREGUNTA);
    }

    /* PISTA */
    function ayuda() {
       Swal.fire({
            title: 'Ayuda',
            text: objetoPregunta.ayuda,
            imageUrl: objetoPregunta.ayudaImg,
            imageHeight: 300
        }); 
    }